const m = require('../lib/index.js');

const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert;

describe('getDiffieHellmanKey', function () {

  // describe('no parameters', function() {
  //   it('key should be buffer', function() {
  //     let c = m.getDiffieHellmanKey();
  //     let buf = c.generateKeys();
  //     expect(buf)
  //       .to.be.instanceof(Buffer)
  //   });
  // });

  describe('base64 encoding', function () {
    it('should be base64 length 1024', async () => {
      try {
        let c = await m.getDiffieHellmanKey();

        let b64 = c.generateKeys('base64');
        expect(b64)
          .to.have.lengthOf(1024);
      } catch (e) {
        return e;
      }

    });



  });

  describe('hex encoding', function () {
    it('should be hex length 1536', async () => {
      try {
        let c = await m.getDiffieHellmanKey();
        let ks = c.generateKeys();
        let hs = c.getPublicKey('hex');

        expect(hs)
          .to.have.lengthOf(1536);
      } catch (e) {
        return e;
      }
    });
  });

  describe('should be rejected', function () {
    it('should be hex length 1536', async () => {
      try {
        let c = await m.getDiffieHellmanKey('error');
        let ks = c.generateKeys();
        let hs = c.getPublicKey('hex');
      } catch (e) {
        assert.isDefined(e);
      }
    });
  });

});