## [Unreleased]
## [0.3.0] - 2018-08-31
### Changed
- Relicensed to Apache 2.0
## [0.2.0] - 2018-04-18
### Fixed
- fixed promise resolving

### Added
- test for promise rejection

## [0.0.2] - 2018-03-13
### Changed
- fixed package.json

## [0.0.1] - 2018-03-13
- initial release
