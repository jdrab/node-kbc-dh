/**
 *
 * @apiparam {String} encoding - if no ecoding is provided Buffer is returned
 *					  possible params are 'latin1', 'hex', or 'base64'
 * @apiparam {String} groupName - default modp17
 * @apiparam {String} crypto module that is compatyble with 'crypto'
 *
 * @apiSuccess {Buffer} Buffer if no encoding is provided as param
 * @apiSuccess {String} New DiffieHellmann key modp17.
 */

const crypto = require("crypto");
// const server_secret = require('server_secret');


async function getDiffieHellmanKey(encoding, groupName = 'modp17', module = crypto) {
  return new Promise((resolve, reject) => {
    try {
      let key = crypto.getDiffieHellman(groupName);
      key.generateKeys(encoding);
      resolve(key);
    } catch (e) {
      reject(e);
    }
  });
}

module.exports = {
  getDiffieHellmanKey: getDiffieHellmanKey,
}